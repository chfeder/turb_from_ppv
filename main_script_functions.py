#!/usr/bin/env python

# import all relevant modules
import os
print("imported os")
import matplotlibrc
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
print("imported matplotlib")
import numpy as np
print("imported numpy")
import argparse
print("imported argparse")
import pdb
print("imported pdb")
import scipy.ndimage

# Get the path of the file to be analysed and check it exists
def get_flash_data_path(sim_id, sphere_test):
	path = "../flash_data/{}/Turb_hdf5_plt_cnt_{}".format(sim_id, sphere_test)

	# make sure the path exists
	if os.path.exists(path) == False:
		print("error: file to analyse does not exist")
		exit()

	print("file path: {}".format(path))
	return path

# Define and, if needed, create the paths for outputs to be saved in
def get_output_path(sim_id, cutoff_str, sphere_test, fwhm, images = False):
	output_path = "../output/{}".format(sim_id)

	# make the data path if it does not exist
	if os.path.exists(output_path) == False:
		os.makedirs(output_path)
		print("made directory: {}".format(output_path))

	# make the directory for storing calculated data if it does not exist
	if os.path.exists("{}/data/{}/{}".format(output_path, sphere_test, fwhm)) == False:
		os.makedirs("{}/data/{}/{}".format(output_path, sphere_test, fwhm))
		print("made directory: {}/data/{}/{}".format(output_path, sphere_test, fwhm))

	# make the directory for storing images if it does not exist and images are being made
	if os.path.exists("{}/images/{}/{}/{}".format(output_path, cutoff_str, sphere_test, fwhm)) == False and images:
		os.makedirs("{}/images/{}/{}/{}".format(output_path, cutoff_str, sphere_test, fwhm))
		print("made directory: {}/images/{}/{}/{}".format(output_path, cutoff_str, sphere_test, fwhm))

	return output_path

def load_data(flash_data_path, output_path, sphere_test, fwhm, overwrite = False, need_all = False):
	# redefine data path
	data_path = "{}/data/{}".format(output_path, sphere_test)
	print("\nextracting predefined data")

	# Initialise the property variables
	properties = {"all_data": None, "*time": None, "*min_max": None, "*disp_data": None,
				"#velx_zero_moment": None, "#vely_zero_moment": None, "#velz_zero_moment": None,
				"#velx_first_moment": None, "#vely_first_moment": None, "#velz_first_moment": None,
				"#velx_second_moment": None, "#vely_second_moment": None, "#velz_second_moment": None,
				"#velx_first_moment_planefit": None, "#vely_first_moment_planefit": None, "#velz_first_moment_planefit": None}

	# extract the defined properties or delete if overwriting
	for key in properties:
		if key[0] == "*":
			file_path = "{}/{}.txt".format(data_path, key[1:])
		if key[0] == "#":
			file_path = "{}/{}/{}.txt".format(data_path, fwhm, key[1:])
		if key[0] == "*" or key[0] == "#":
			if os.path.exists(file_path):
				if overwrite:
					print("deleting {}".format(file_path))
					os.remove(file_path)
				else:
					print("extracting {}".format(file_path))
					properties[key] = np.genfromtxt(file_path, delimiter = ',')

	# load the yt data if needed
	if len([x for x in properties.keys() if properties[x] is None]) > 1:
		if need_all:
			print("not all properties defined for dump {}".format(sphere_test))
			pdb.set_trace()
			exit()
		else:
			print("not all properties defined")
			print("importing yt and loading data")
			import yt
			properties["all_data"] = yt.load(flash_data_path)

	# make dictionaries for moment properties along each line of sight
	x = {"vel": "velx",
		"zero_moment": properties["#velx_zero_moment"],
		"first_moment": properties["#velx_first_moment"],
		"second_moment": properties["#velx_second_moment"],
		"planefit": properties["#velx_first_moment_planefit"]}
	y = {"vel": "vely",
		"zero_moment": properties["#vely_zero_moment"],
		"first_moment": properties["#vely_first_moment"],
		"second_moment": properties["#vely_second_moment"],
		"planefit": properties["#vely_first_moment_planefit"]}	
	z = {"vel": "velz",
		"zero_moment": properties["#velz_zero_moment"],
		"first_moment": properties["#velz_first_moment"],
		"second_moment": properties["#velz_second_moment"],
		"planefit": properties["#velz_first_moment_planefit"]}

	return properties, x, y, z

# Process the data, extracting a particular attribute
def extract_data(attribute, all_data):
	print("extracting {} data from flash data".format(attribute))
	level = all_data.max_level
	dims = all_data.domain_dimensions * all_data.refine_by ** level
	dims = [dims[0], dims[1], dims[2]]
	cube = all_data.covering_grid(level, left_edge=all_data.domain_left_edge, dims=dims)
	return np.array(cube[attribute])
	
# Smoothing a 2D array with a Gaussian kernel (sigma or fwhm in pixel units)
def gauss_smooth(input_map, sigma=None, fwhm=None, mode='nearest'):
    if sigma is None and fwhm is None:
        print("Either sigma or fwhm must be specified for Gaussian beam smoothing.")
        import pdb; pdb.set_trace()
    if sigma is not None and fwhm is not None:
        print("Cannot set both sigma or fwhm; specify either sigma or fwhm for Gaussian beam smoothing.")
        import pdb; pdb.set_trace()
    # work out the input sigma for the scipy smoothing function
    sigma_in = 0.0
    if sigma is not None:
        sigma_in = sigma
    if fwhm is not None:
        sigma_in = fwhm / (2.0*np.sqrt(2.0*np.log(2.0))) # convert FWHM to sigma
    # make a corresponding unit map and turn nans to zeros]
    input_map_zero = input_map.copy()
    input_map_zero[np.isnan(input_map)] = 0
    standard_map = 0 * input_map_zero.copy() + 1
    standard_map[np.isnan(input_map)] = 0
    # smooth auxillary maps
    smoothed_input = scipy.ndimage.gaussian_filter(input_map_zero, sigma=sigma_in, order=0, mode=mode)
    smoothed_standard = scipy.ndimage.gaussian_filter(standard_map, sigma=sigma_in, order=0, mode=mode)
    smoothed_input[np.isnan(input_map)] = np.nan
    smoothed_standard[np.isnan(input_map)] = np.nan
    # make final map and put nans back
    smoothed_map = smoothed_input / smoothed_standard
    return smoothed_map

# Create the nth moment map data
def moment_data(line_of_sight, velocity_component, density, n, fwhm = None):
	print("calculating moment map {} along axis {}".format(n, line_of_sight))

	if line_of_sight == 'x':
		dimension = 0
	elif line_of_sight == 'y':
		dimension = 1
	elif line_of_sight == 'z':
		dimension = 2
	else:
		raise RuntimeError ("Axis to make slices along was not specified as x, y, or z")

	# calculate the numerator and denominator of the moment fraction
	numerator = np.nansum(np.power(velocity_component, n) * density, axis = dimension)
	denominator = np.nansum(density, axis = dimension)

	# if given a fwhm, apply the beam smoothing function
	if fwhm is not None:
		numerator = gauss_smooth(numerator, fwhm = fwhm)
		denominator = gauss_smooth(denominator, fwhm = fwhm)

	# calculate the moment accoverding depending on if zero moment
	if n == 0:
		moment_map = np.divide(numerator, 232)
	elif n == 1:
		moment_map = np.divide(numerator, denominator)
	elif n == 2:
		first_mom = moment_data(line_of_sight, velocity_component, density, 1, fwhm = fwhm)
		moment_map = (np.divide(numerator, denominator) - first_mom**2)**0.5

	return moment_map

# Creates a planefit to a set of 2D points, taking into account the ambient space
def idl_gradient_fit(zin, weights = None):
	zin = np.array(zin)
	weights = np.array(weights)

	# Find the number of x values and the number of y values of zin
	xn = np.shape(zin)[1]
	yn = np.shape(zin)[0]

	# Create x and y arrays corresponding to coordinates of z
	xin = np.tile(np.array(range(xn), dtype = float), (yn,1))
	yin = np.tile(np.array(range(yn), dtype = float).reshape(yn,1), (1, xn)).reshape(yn, xn)

	# Replace indices for which zin is a nan with nans for x and y
	xin[np.isnan(zin)] = np.nan
	yin[np.isnan(zin)] = np.nan

	# Get the effective size of z
	z_size = zin.size
	num_nans = (np.isnan(zin)).sum()
	n = z_size - num_nans

	# If there is not enough data, return the average of the given data
	if n < 3:
		print("planefit: too few points")
		r = np.nanmean(zin)
		return r

	# Shift x, y, z to the center of gravity frame
	x0 = float(np.nansum(xin)) / n
	y0 = float(np.nansum(yin)) / n
	z0 = float(np.nansum(zin)) / n

	x = xin - x0
	y = yin - y0
	z = zin - z0

	# Check if have the weights
	if weights is not None:
		s = np.nansum(weights)

		sx = np.nansum(weights * x)
		sy = np.nansum(weights * y)
		sz = np.nansum(weights * z)

		sxx = np.nansum(weights * x * x)
		syy = np.nansum(weights * y * y)
		
		sxy = np.nansum(weights * x * y)
		sxz = np.nansum(weights * x * z)
		syz = np.nansum(weights * y * z)

	else:
		s = float(n)
		sx = np.nansum(x)
		sy = np.nansum(y)
		sz = np.nansum(z)

		sxx = np.nansum(x * x)
		syy = np.nansum(y * y)
		
		sxy = np.nansum(x * y)
		sxz = np.nansum(x * z)
		syz = np.nansum(y * z)

	D = s * (sxx * syy - sxy * sxy) + sx * (sxy * sy - sx * syy) + sy * (sx * sxy - sxx * sy)

	r0 = sz * (sxx * syy - sxy * sxy) + sx * (sxy * syz - sxz * syy) + sy * (sxz * sxy - sxx * syz)
	r1 = s * (sxz * syy - sxy * syz) + sz * (sxy * sy - sx * syy)   + sy * (sx * syz - sxz * sy)
	r2 = s * (sxx * syz - sxz * sxy) + sx * (sxz * sy - sx * syz)   + sz * (sx * sxy - sxx * sy)
	r = np.array([r0, r1, r2]) / D

	# Now shift x, y, and z back
	r[0] = r[0] + z0 - r[1] * x0 - r[2] * y0

	planefit = r[0] + r[1] * xin + r[2] * yin

	return planefit

# Set the limits of an axis or colour bar to extend the range by 0.5%
def set_limit(x, max_or_min):
	if max_or_min == "max":
		if x > 0:
			return 1.05 * x
		else:
			return 0.95 * x
	if max_or_min == "min":
		if x > 0:
			return 0.95 * x
		else:
			return 1.05 * x

# Make and save an image of a moment map
def make_image(points, filename, imagename, cutoff_str, sphere_test, title, n, axes, colour_bar_label, ax_lim, fwhm):
    x_label = "{} (pc)".format(axes[0])
    y_label = "{} (pc)".format(axes[1])
    max_points = np.nanmax(points)
    min_points = np.nanmin(points)
    
    font_colour = 'black'
        
    if n == 1:
        absmax = max(abs(max_points), abs(min_points))
        amax = 1.02 * absmax
        amin = -1.02 * absmax
        cmap = plt.get_cmap('RdBu').reversed()
    
    if n == 0 or n == 2:
        amax = set_limit(max_points, "max")
        amin = set_limit(min_points, "min")
                
    if n == 0:
        cmap = plt.get_cmap('plasma')
    
    if n == 2:
        cmap = plt.get_cmap('viridis')
        
    fig, ax = plt.subplots()
            
    font_colour = 'black'
                                
    if n == 0:
        image = ax.imshow(np.rot90(points), norm = LogNorm(), aspect = 1, extent = (ax_lim[0], ax_lim[1], ax_lim[2], ax_lim[3]), cmap = cmap)
    else:
        image = ax.imshow(np.rot90(points), aspect = 1, extent = (ax_lim[0], ax_lim[1], ax_lim[2], ax_lim[3]), cmap = cmap)
                                                
    ax.text(0.03, 0.97, title, fontweight = 'bold', color = font_colour, horizontalalignment = 'left', verticalalignment = 'top', transform = ax.transAxes)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    cb1 = fig.colorbar(image, ax = ax, fraction = 0.046, pad = 0.04)
    cb1.set_label(colour_bar_label, rotation = 90, position = (1,0.5))
    if n == 0:
        cb1.ax.minorticks_off()
            
    plt.savefig("{}/images/{}/{}/{}/{}.pdf".format(filename, cutoff_str, sphere_test, fwhm, imagename))
    plt.clf()
    plt.close()
    print("saved {} to {}/images/{}/{}/{}/{}.pdf".format(imagename, filename, cutoff_str, sphere_test, fwhm, imagename))

# Make a 3-panel moment map image including the first moment, the gradient and the gradient subtracted first moment map
def make_moments_image(image_name, output_path, first_moment_map, gradient_fit, subtracted_gradient, axes, cutoff_str, sphere_test, fwhm, ax_lim):
    x_label = "{} (pc)".format(axes[0])
    y_label = "{} (pc)".format(axes[1])
    
    # Determine the maximum and minimum of the three plots for uniform colourbar use
    absmax1 = max(abs(np.nanmax(first_moment_map)), abs(np.nanmin(first_moment_map)))
    absmax2 = max(abs(np.nanmax(gradient_fit)), abs(np.nanmin(gradient_fit)))
    absmax3 = max(abs(np.nanmax(subtracted_gradient)), abs(np.nanmin(subtracted_gradient)))
    total_max = max([absmax1, absmax2, absmax3])
        
    title1 = "Before subtracting \ngradient"
    title2 = "Fitted gradient"
    title3 = "After subtracting \ngradient"
        
    fig, (ax1, ax2, ax3) = plt.subplots(1,3, figsize = (40, 12))
        
    image1 = ax1.imshow(np.rot90(first_moment_map), aspect = 1, cmap = plt.get_cmap('viridis'), extent = (ax_lim[0], ax_lim[1], ax_lim[2], ax_lim[3]), vmin = -total_max, vmax = total_max)
    ax1.text(0.02,0.98, title1, fontsize = 20, fontweight = 'bold',
        horizontalalignment = 'left', verticalalignment = 'top', transform = ax1.transAxes)
    ax1.set_xlabel(x_label, fontsize = 20)
    ax1.set_ylabel(y_label, fontsize = 20)
    cb1 = fig.colorbar(image1, ax = ax1, fraction=0.046, pad=0.04)
    cb1.set_label("Velocity (km/s)", rotation=90, position=(1,0.5), fontsize = 18)
                 
    image2 = ax2.imshow(np.rot90(gradient_fit), aspect = 1, cmap = plt.get_cmap('viridis'), extent = (ax_lim[0], ax_lim[1], ax_lim[2], ax_lim[3]), vmin = -total_max, vmax = total_max)
    ax2.text(0.02,0.98, title2, fontsize = 20, fontweight = 'bold',
        horizontalalignment = 'left', verticalalignment = 'top', transform = ax2.transAxes)
    ax2.set_xlabel(x_label, fontsize = 20)
    ax2.set_ylabel(y_label, fontsize = 20)
    cb2 = fig.colorbar(image2, ax = ax2, fraction = 0.046, pad = 0.04)
    cb2.set_label("Velocity (km/s)", rotation=90, position=(1,0.5))
                 
    image3 = ax3.imshow(np.rot90(subtracted_gradient), aspect = 1, cmap = plt.get_cmap('viridis'), extent = (ax_lim[0], ax_lim[1], ax_lim[2], ax_lim[3]), vmin = -total_max, vmax = total_max)
    ax3.text(0.02,0.98, title3, fontsize = 20, fontweight = 'bold', horizontalalignment = 'left', verticalalignment = 'top', transform = ax3.transAxes)
    ax3.set_xlabel(x_label)
    ax3.set_ylabel(y_label)
    cb3 = fig.colorbar(image3, ax = ax3, fraction = 0.046, pad = 0.04)
    cb3.set_label("Velocity (km/s)", rotation=90, position=(1,0.5))
                 
    plt.savefig("{}/images/{}/{}/{}/{}".format(output_path, cutoff_str, sphere_test, fwhm, image_name))
    plt.clf()
    plt.close()
    print("saved {} to {}/images/{}/{}/{}/{}".format(image_name, cutoff_str, output_path, sphere_test, fwhm, image_name))

# Calculate the centre of mass of a 3-dimensional mass
def centre_of_mass_coordinate(density, coordinates, axis):
    if axis == 'x':
        flattened_density = [np.nansum(density[i, :, :]) for i in range(256)]
    if axis == 'y':
        flattened_density = [np.nansum(density[:, i, :]) for i in range(256)]
    if axis == 'z':
        flattened_density = [np.nansum(density[:, :, i]) for i in range(256)]
    return np.nansum(flattened_density * coordinates) / np.nansum(flattened_density)

# return cell-centered coordinates | . | . |
#                               xmin       xmax
def get_1d_coords(cmin=0, cmax=1, ndim=2):
    d = (cmax-cmin) / np.float(ndim)
    c = np.arange(cmin+d/2, cmax+d/2, d)
    return c

# Create a 3-dimensional coordinate system
def get_3d_coords(cmin=[0,0,0], cmax=[1,1,1], ndim=[2,2,2]):
    cmin = np.array(cmin)
    cmax = np.array(cmax)
    ndim = np.array(ndim)
    if cmin.ndim != 1: cmin = [cmin,cmin,cmin]
    if cmax.ndim != 1: cmax = [cmax,cmax,cmax]
    if ndim.ndim != 1: ndim = [ndim,ndim,ndim]
    c0 = get_1d_coords(cmin=cmin[0], cmax=cmax[0], ndim=ndim[0])
    c1 = get_1d_coords(cmin=cmin[1], cmax=cmax[1], ndim=ndim[1])
    c2 = get_1d_coords(cmin=cmin[2], cmax=cmax[2], ndim=ndim[2])
    c0c, c1c, c2c = np.meshgrid(c0, c1, c2, indexing='ij')
    return c0c, c1c, c2c, c0, c1, c2

def get_3d_coords_old(xmin=0, xmax=100, ymin=0, ymax=100, zmin=0, zmax=100):
    x = np.linspace(xmin,xmax,256)
    y = np.linspace(ymin,ymax,256)
    z = np.linspace(zmin,zmax,256)
    yc, xc, zc = np.meshgrid(x,y,z)
    return xc, yc, zc, x, y, z

# Calculate the 3-dimensional velocity dispersion and 3-dimensional rotation corrected velocity dispersion
def dispersion(omega, density, velx, vely, velz, min_max):
    # Make a 3-dimensional grid of spatial coordinates
    xc, yc, zc, x, y, z = get_3d_coords(cmin=[min_max[0],min_max[2],min_max[4]], cmax=[min_max[1],min_max[3],min_max[5]], ndim=[256,256,256])
    
    # Find the centre of mass (acts differently (strangely maybe) for 0 and 1 dumps)
    comx = centre_of_mass_coordinate(density, x, 'x')
    comy = centre_of_mass_coordinate(density, y, 'y')
    
    # Find the distance from the rotation axis assuming the rotation axis is parallel to the z-axis at the centre of mass
    xc_corr = xc - comx
    yc_corr = yc - comy
        
    # Set cells outside of the cloud to zero
    velx[np.isnan(density)] = np.nan
    vely[np.isnan(density)] = np.nan
    velz[np.isnan(density)] = np.nan
        
    # Make a 3-dimensional grid of (velx, vely)
    vx_new = velx[:,:,:,np.newaxis]
    vy_new = vely[:,:,:,np.newaxis]
    v_plane = np.concatenate((vx_new, vy_new), axis = 3)
        
    # Find the rotational velocity
    v_rot = omega * np.concatenate((-1 * yc_corr[:,:,:,np.newaxis], xc_corr[:,:,:,np.newaxis]), axis = 3)
    v_rot *= 3.0857e18 * 1e-5 # Convert to m/s (from pc/s)
    v_rot[np.isnan(density)] = np.nan
    v_cor = v_plane - v_rot
        
    disp_x = np.nanstd(velx)
    disp_y = np.nanstd(vely)
    disp_z = np.nanstd(velz)
    disp_x_corr = np.nanstd(v_cor[:,:,:,0])
    disp_y_corr = np.nanstd(v_cor[:,:,:,1])
    disp_3d = np.sqrt(float(disp_x) ** 2 + float(disp_y) ** 2 + float(disp_z) ** 2)
    disp_3d_corr = np.sqrt(float(disp_x_corr) ** 2 + float(disp_y_corr) ** 2 + float(disp_z) ** 2)
        
    print('x dispersion (uncorrected): {}'.format(disp_x))
    print('y dispersion (uncorrected): {}'.format(disp_y))
    print('x dispersion (corrected): {}'.format(disp_x_corr))
    print('y dispersion (corrected): {}'.format(disp_y_corr))
    print('z dispersion (corrected/uncorrected): {}'.format(disp_z))
    print('3d dispersion (uncorrected): {}'.format(disp_3d))
    print('3d dispersion (corrected): {}'.format(disp_3d_corr))
    
    #import ipdb; ipdb.set_trace()
        
    return np.array([disp_x, disp_y, disp_z, disp_x_corr, disp_y_corr, disp_3d, disp_3d_corr])
	
# Write the data to a text file
# x, y and z are arrays containing the moment data
def output_data(time, disp_array, x, y, z, filename):
    f = open(filename, 'w+')
    f.write(filename)
    
    f.write("\nTime = {}s \n".format(str(time[0])))
        
    f.write('\nLine of Sight: x \n____________________\n')
    f.write("sigma(<v>) = {}km/s\n".format(np.nanstd(x["first_moment"])))
    f.write("sigma(<v> - grad) = {}km/s \n".format(np.nanstd(x["reduced"])))
    f.write("mean of second moment = {}km/s \n".format(np.nanmean(x["second_moment"])))
    f.write("sigma_i**2 = {} (km/s)**2 \n".format(np.nanmean(x["second_moment"]**2)))
        
    f.write('\nLine of Sight: y \n____________________\n')
    f.write("sigma(<v>) = {}km/s \n".format(np.nanstd(y["first_moment"])))
    f.write("sigma(<v> - grad) = {}km/s \n".format(np.nanstd(y["reduced"])))
    f.write("mean of second moment = {}km/s \n".format(np.nanmean(y["second_moment"])))
    f.write("sigma_i**2 = {} (km/s)**2 \n".format(np.nanmean(y["second_moment"]**2)))
        
    f.write('\nLine of Sight: z\n____________________\n')
    f.write("sigma(<v>) = {}km/s \n".format(np.nanstd(z["first_moment"])))
    f.write("sigma(<v> - grad) = {}km/s \n".format(np.nanstd(z["reduced"])))
    f.write("mean of second moment = {}km/s \n".format(np.nanmean(z["second_moment"])))
    f.write("sigma_i**2 = {} (km/s)**2 \n".format(np.nanmean(z["second_moment"]**2)))
    
    f.write('\nTotal \n____________________\n')
    f.write("sigma(<|v|>) = {}km/s \n".format(disp_array[5]))
    f.write("sigma(<|v|>) rotation corrected = {}km/s \n".format(disp_array[6]))
    f.write("vx dispersion (uncorrected) = {}km/s \n".format(disp_array[0]))
    f.write("vy dispersion (uncorrected) = {}km/s \n".format(disp_array[1]))
    f.write("vz dispersion (uncorrected) = {}km/s \n".format(disp_array[2]))
    f.write("vx dispersion (corrected) = {}km/s \n".format(disp_array[3]))
    f.write("vy dispersion (corrected) = {}km/s \n".format(disp_array[4]))
    
    f.close()
    print("wrote output data to {}".format(filename))
