#!/usr/bin/env python

# Get Data
# Extracts the data (time, standard deviation of the first moment map, standard deviation of the gradient corrected first moment map,
# and mean of the second moment map along the x-axis, y-axis and z-axis lines of sights, along with the three dimensional velocity
# dispersion and the three dimensional rotation corrected velocity dispersion) for each consecutive dump between the start and end dumps
# specified. Returns a numpy file.

import numpy as np
import os
import pdb
import argparse
#import file_handling_functions as fhf

parser = argparse.ArgumentParser()
parser.add_argument('--start_time', default = 0, help = "Simtulation time step to start analysing from, default is 0", type = int)
parser.add_argument('end_time', default = 88, help = "Simulation time step to stop analysing at, default = 88", type = int)
parser.add_argument('--fwhm', default = 1.1636, help = "Telescope fwhm in pixels, default is 1.1636 (1 pc)", type = float)
parser.add_argument('--sim_id', default = "high_rot", help = "simulation identification, default is high_rot", type = str)
parser.add_argument('--cutoff', default = 0.1, help = "Cut off every cell with a value less than this in the zero moment contrast map", type = float)
parser.add_argument('--print_data', help = "print all values stored as they are added", action="store_true")
args = parser.parse_args()

# Extract the number from a line 
def extract_number(i, contents, end_string = "km/s", print_data = False):
        line = contents[i]

        i_start = line.find("=")
        i_end = line.rfind(end_string)

        number = float(line[i_start+1:i_end])
        if print_data:
                print("{}: {}".format(i, line[:-1]))

        return number

# define the data path and stop if this does not exist
data_path = "../output/{}/data".format(args.sim_id)
if os.path.exists(data_path) == False:
	print("error: data path {} does not exist").format(data_path)
	exit()

# make the cutoff string
if args.cutoff is None:
	cutoff_str = "no_cutoff"
else:
	cutoff_str = str(args.cutoff)

total_data = []

for i in range(args.start_time, args.end_time + 1):
        number = str(i).zfill(4)
        file = "Turbulence_Data_{}_r{}_{}.txt".format(number, args.fwhm, cutoff_str)
        data = []
        
        if os.path.exists("{}/{}/{}".format(data_path, number, file)):
                f = open("{}/{}/{}".format(data_path, number, file), 'r')
                contents = f.readlines()
                
                # extract time
                data.append(extract_number(1, contents, end_string = "s", print_data = args.print_data))

                # extract x-axis line of sight data
                for j in range(5,8):
                        number = extract_number(j, contents, print_data = args.print_data)
                        data.append(number)
                number = extract_number(8, contents, end_string="(km/s)**2", print_data = args.print_data)
                data.append(number)

                # extract y-axis line of sight data
                for j in range(12,15):
                        number = extract_number(j, contents, print_data = args.print_data)
                        data.append(number)
                number = extract_number(15, contents, end_string="(km/s)**2", print_data = args.print_data)
                data.append(number)

                # extract z-axis line of sight data
                for j in range(19,22):
                        number = extract_number(j, contents, print_data = args.print_data)
                        data.append(number)
                number = extract_number(22, contents, end_string="(km/s)**2", print_data = args.print_data)
                data.append(number)

                # extract total data
                for j in range(26,33):
                        number = extract_number(j, contents, print_data = args.print_data)
                        data.append(number)
                f.close()

                total_data.append(data)
                print("analysed {}".format(file))
                
        else:
                print("data not found for dump {} at r{}".format(number, args.fwhm))

np.savetxt("{}/total_turbulence_data_r{}_{}.txt".format(data_path, args.fwhm, cutoff_str), total_data, delimiter = ',')
print("wrote total turbulence data to {}/total_turbulence_data_r{}_{}.txt".format(data_path, args.fwhm, cutoff_str))
