#!/usr/bin/env python

# import all relevant modules
import numpy as np
print("imported numpy")
import argparse
print("imported argparse")
import pdb
print("imported pdb")
import main_script_functions as msf
print("imported main_script_functions")

################################################################################################################################################################
################################################################### Data Analysis Function #####################################################################
################################################################################################################################################################
def data_analysis(sim_id, sphere_test, cutoff_str, fwhm, images):
	# define necessary directories and load data
	flash_data_path = msf.get_flash_data_path(sim_id, sphere_test)
	output_path =  msf.get_output_path(sim_id, cutoff_str, sphere_test, fwhm, images)
	properties, x, y, z = msf.load_data(flash_data_path, output_path, sphere_test, fwhm, overwrite = not images)
	
	# find the time
	if properties["*time"] is None:
		properties["*time"] = np.array([float(properties["all_data"].current_time)])
		np.savetxt("{}/data/{}/time.txt".format(output_path, sphere_test), properties["*time"], delimiter = ',')
		print("wrote {}/data/{}/time.txt".format(output_path, sphere_test))
	
	# set the minimum and maximum values of each axis
	if properties["*min_max"] is None:
		xmin, ymin, zmin = properties["all_data"].domain_left_edge / (3.086e+18)
		xmax, ymax, zmax = properties["all_data"].domain_right_edge / (3.086e+18)    
		properties["*min_max"] = np.array([xmin, xmax, ymin, ymax, zmin, zmax])
		np.savetxt("{}/data/{}/min_max.txt".format(output_path, sphere_test), properties["*min_max"], delimiter = ',')
		print("wrote {}/data/{}/min_max.txt".format(output_path, sphere_test))
	
	# determine if extracting the density is necessary
	density_reliant_properties = np.array([x["first_moment"], 	y["first_moment"], 	z["first_moment"],
											x["planefit"], 		y["planefit"], 		z["planefit"],
											x["second_moment"], y["second_moment"], z["second_moment"],
											x["zero_moment"], 	y["zero_moment"], 	z["zero_moment"], properties["*disp_data"]])
	density_required = any([p is None for p in density_reliant_properties])
	
	# extract and filter weighting attribute data if needed
	if density_required:
		density = msf.extract_data('dens', properties["all_data"])
		mask_density = density < 1.62e-21
		density[mask_density] = np.nan
	
	# get first moment map, second moment map, pdf and values for velx, vely, velz
	for los in [x, y, z]:
		# set the x-axis and y-axis for produced images to the appropriate cloud axes
		axis = los["vel"][3]
		axes = np.array(['x','y','z'])
		axes_limits = np.array([p for p in properties["*min_max"]])
		axis_ind = np.where(axes == axis)
		np.delete(axes, axis_ind)
		np.delete(axes_limits, axis_ind)
		np.delete(axes_limits, axis_ind)
	
		### calculate the moment map data if needed ###############################################################################################################
		# if needed, extract velocity data and convert to km/s
		if any([moment is None for moment in [los["zero_moment"], los["first_moment"], los["second_moment"], los["planefit"], properties["*disp_data"]]]):
			los["vel"] = msf.extract_data(los["vel"], properties["all_data"]) / 1e5
	
		# make the zero moment map
		if los["zero_moment"] is None:
			los["zero_moment"] = msf.moment_data(axis, los["vel"], density, 0, fwhm = fwhm)
			np.savetxt("{}/data/{}/{}/vel{}_zero_moment.txt".format(output_path, sphere_test, fwhm, axis), los["zero_moment"], delimiter = ',')
			print("saved {}/data/{}/{}/vel{}_zero_moment.txt".format(output_path, sphere_test, fwhm, axis))
	
		# make first moment map data
		if los["first_moment"] is None or los["planefit"] is None:
			los["first_moment"] = msf.moment_data(axis, los["vel"], density, 1, fwhm = fwhm)
			np.savetxt("{}/data/{}/{}/vel{}_first_moment.txt".format(output_path, sphere_test, fwhm, axis), los["first_moment"], delimiter = ',')
			print("saved {}/data/{}/{}/vel{}_first_moment.txt".format(output_path, sphere_test, fwhm, axis))
	
		# make second moment map data
		if los["second_moment"] is None:
			los["second_moment"] = msf.moment_data(axis, los["vel"], density, 2, fwhm = fwhm)
			np.savetxt("{}/data/{}/{}/vel{}_second_moment.txt".format(output_path, sphere_test, fwhm, axis), los["second_moment"], delimiter = ',')
			print("saved {}/data/{}/{}/vel{}_second_moment.txt".format(output_path, sphere_test, fwhm, axis))
	
		# make the first moment map plane fit
		if los["planefit"] is None:
			los["planefit"] = msf.idl_gradient_fit(los["first_moment"], weights = density)
			np.savetxt("{}/data/{}/{}/vel{}_first_moment_planefit.txt".format(output_path, sphere_test, fwhm, axis), los["planefit"], delimiter = ',')
			print("saved {}/data/{}/{}/vel{}_first_moment_planefit.txt".format(output_path, sphere_test, fwhm, axis))
	
		# make gradient corrected first moment map
		los["reduced"] = los["first_moment"] - los["planefit"]
	
		# apply the cutoff if it is not none
		if args.cutoff is not None:
			zero_moment_contrast = np.divide(los["zero_moment"], np.nanmean(los["zero_moment"]))
			mask = zero_moment_contrast < args.cutoff
	
			los["zero_moment"][mask] = np.nan
			los["first_moment"][mask] = np.nan
			los["planefit"][mask] = np.nan
			los["reduced"][mask] = np.nan
			los["second_moment"][mask] = np.nan
	
		### make and save moment map images if desired #################################################################################################################
		if images:
			# zero moment map
			title = r'$\mathrm{0}$-$\mathrm{moment}$'
			imagename = "Turb{}{}ZeroMomentMapLineOfSight{}r{}".format(sphere_test, los["vel"], axis, fwhm)
			colour_bar_label = r'$\mathrm{Density \; (g/cm^3)}$'
			msf.make_image(los["zero_moment"], output_path, imagename, cutoff_str, sphere_test, title, 0, axes, colour_bar_label, axes_limits, fwhm)
			
			# first moment map
			title = "Before subtracting \ngradient"
			imagename = "Turb{}{}FirstMomentMapLineOfSight{}r{}".format(sphere_test, los["vel"], axis, fwhm)
			colour_bar_label = "$v_{}$ (km/s)".format(axis)
			msf.make_image(los["first_moment"], output_path, imagename, cutoff_str, sphere_test, title, 1, axes, colour_bar_label, axes_limits, fwhm)
	
			# first moment map plane fit
			title = "Fitted gradient"
			imagename = "Turb{}{}PlaneFitLineOfSight{}r{}".format(sphere_test, los["vel"], axis, fwhm)
			colour_bar_label = "$v_{}$ (km/s)".format(axis)
			msf.make_image(los["planefit"], output_path, imagename, cutoff_str, sphere_test, title, 1, axes, colour_bar_label, axes_limits, fwhm)
	
			# gradient corrected first moment map
			title = "After subtracting \ngradent"
			imagename = "Turb{}{}GradientSubtractedLineOfSight{}r{}".format(sphere_test, los["vel"], axis, fwhm)
			colour_bar_label = "$v_{}$ (km/s)".format(axis)
			msf.make_image(los["reduced"], output_path, imagename, cutoff_str, sphere_test, title, 1, axes, colour_bar_label, axes_limits, fwhm)
	
			# second moment map
			title = "Second Moment Map"
			imagename = "Turb{}{}SecondMomentMapLineOfSight{}r{}".format(sphere_test, los["vel"], axis, fwhm)
			colour_bar_label = "$\sigma_{{v_{}}}$ (km/s)".format(axis)
			msf.make_image(los["second_moment"], output_path, imagename, cutoff_str, sphere_test, title, 2, axes, colour_bar_label, axes_limits, fwhm)
			
			# first moment map, plane fit and gradient corrected first moment map
			image_name = "Turb{}vel{}FirstMomentMapsr{}.pdf".format(sphere_test, axis, fwhm)
			msf.make_moments_image(image_name, output_path, los["first_moment"], los["planefit"], los["reduced"], axes, cutoff_str, sphere_test, fwhm, axes_limits)
	
		print("finished {} loop\n".format(axis))
	
	# correct for cutoff
	if properties["*disp_data"] is None:
		# find omega
		if sim_id in ["with_turb", "no_turb"]:
			omega = 3.02567e-14
		elif sim_id == "high_rot":
			omega = 5.124e-14
		elif sim_id == "turb_only":
			omega = 0
		else:
			omega = float(input("sim_id is not 'with_turb, 'no_turb', 'turb_only' or 'high_rot'; please enter omega: "))
	
		# make and save disp_data
		properties["*disp_data"] = msf.dispersion(omega, density, x["vel"], y["vel"], z["vel"], properties["*min_max"])
		np.savetxt("{}/data/{}/disp_data.txt".format(output_path, sphere_test), properties["*disp_data"], delimiter = ',')
	
	# output data
	filename = "{}/data/{}/Turbulence_Data_{}_r{}_{}.txt".format(output_path, sphere_test, sphere_test, fwhm, cutoff_str)
	msf.output_data(properties["*time"], properties["*disp_data"], x, y, z, filename)

###############################################################################################################################################################
######################################################################### Main Code ###########################################################################
###############################################################################################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('sphere_test', help = "Identity of sphere test", type = int)
parser.add_argument('--upto', default = None, help = "analyse all data from the given sphere test to this sphere_test, if left blank then only the given sphere test is analysed", type = int)
parser.add_argument('--fwhm', default = 1.1636, help = "Beam fwhm in number of pixels, use 'None' for no smoothing, default is 1.1636 (0.1 pc)", type = float) # make default resolution the 0.1 pc (1.1636 pixels), also do 0.02, 0.04, 0.08, 0.16, 0.32, 0.64, 1.28
parser.add_argument('--images', help = "make and save the moment map images", action = "store_true")
parser.add_argument('--sim_id', default = "high_rot", help = "simulation identification, default is high_rot", type = str)
parser.add_argument('--cutoff', default = 0.1, help = "Cut off every cell with a value less than this in the zero moment contrast map, default is 0.1", type = float)
args = parser.parse_args()

# make the cutoff string
if args.cutoff is None:
	cutoff_str = "no_cutoff"
else:
	cutoff_str = str(args.cutoff)

# analyse either a single dump or all dumps in the given range
if args.upto is not None:
	for dump in range(args.sphere_test, args.upto + 1):
		sphere_test = str(dump).zfill(4)
		print("\nprocessing dump {} with decrease resolution factor {} from simulation {}".format(sphere_test, args.fwhm, args.sim_id))
		data_analysis(args.sim_id, sphere_test, cutoff_str, args.fwhm, args.images)	
else:
	sphere_test = str(args.sphere_test).zfill(4)
	print("\nprocessing dump {} with decrease resolution factor {} from simulation {}".format(sphere_test, args.fwhm, args.sim_id))
	data_analysis(args.sim_id, sphere_test, cutoff_str, args.fwhm, args.images)
